const http = require('http');

const response = function(req, res) {
    
    // simulando requisições de rotas no serivdor http
    if (req.url == '/produtos'){
        res.end('<h1>Listagem de Produtos</h1>');
    } else {
        res.end('<h1>Home do nosso Site!</h1>');
    }
    
};

const server = http.createServer(response);

server.listen(8080);
console.log('Servidor rodando na porta 8080');