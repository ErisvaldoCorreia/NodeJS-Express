// simples exemplo de construção de servidor com http

// importando módulo responsavel pelo servidor
const http = require('http');

// função que devolve uma resposta ao browser
const response = function(req, res) {
    res.end('<h1>Servidor Rodando</h1>');
};

// montagem do servidor
const server = http.createServer(response);

// definição da porta de escuta do servidor
server.listen(8080);
console.log('Servidor rodando na porta 8080');