
module.exports = (app) => {

    app.get('/', (req, res) => {
        
        /* VERIFICAR POSTERIOR ERRO DE COMUNICAÇÃO COM MYSQL
        
        const mysql = require('mysql');

        const connection = mysql.createConnection({
            host:'localhost',
            user:'root',
            password:'',
            database:'bancoTeste',
        });

        connection.query('select * from livros', function(err, results){
            res.render('produtos/lista', {lista:results});
        });

        connection.end();

        */

        // TEMPORARIO PARA SERVIR NA TABELA EJS
        const lista = [
            {
                id:'1',
                nome:'Livro NodeJS',
                descricao:'Livro sobre fundamentos de Node',
                preco:'40.00'
            },
            {
                id:'2',
                nome:'Livro Angular',
                descricao:'Livro sobre fundamentos de Angular',
                preco:'40.00'
            }
        ];

        res.render('produtos/lista', {lista:lista});
    });

}