// modulo de configuração do Express

module.exports = () => {
    
    const express = require('express');
    const app = express();
    // definindo a engine dos templates
    app.set('view engine', 'ejs');
    app.set('views', './src/views');
    console.log('Modulo express carregado!');
    
    return app;

}
