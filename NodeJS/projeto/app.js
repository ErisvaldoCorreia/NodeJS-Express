// construindo servidor com Express
const app = require('./config/express')();
const rotaProd = require('./src/routes/prods')(app);

// porta de execução
app.listen(8000, () => {
    console.log('Servidor rodando');
});
